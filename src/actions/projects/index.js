import { TOAST } from '../../components/Toast/constants';
import { GET_PROJECTS_FAILURE, GET_PROJECTS_SUCCESS } from '../../constants/projects';
import { Api } from '../api/index';

export const getUserProjects = userId => {
  return dispatch => {
    return Api.get(`/user-project/${userId}`)
      .then(({ data }) => {
        return dispatch({
          type: GET_PROJECTS_SUCCESS,
          projects: data,
        });
      })
      .catch(error => {
        TOAST.erro('Erro ao buscar os projetos do usuário!');
        return dispatch({ type: GET_PROJECTS_FAILURE });
      });
  };
};
