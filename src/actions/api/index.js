import axios from 'axios';
import { adicionaTokenNoHeader } from './middlewares';

const Api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

Api.interceptors.request.use(adicionaTokenNoHeader);

export { Api };
