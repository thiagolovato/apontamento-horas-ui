export const adicionaTokenNoHeader = config => {
  const token = localStorage.getItem('token');
  config.headers.Authorization = 'Bearer ' + token;
  return config;
};
