import { Api } from '../api/index';
import { LOGIN_SUCCESSFUL, LOGIN_UNSUCCESSFUL, LOGOUT } from '../../constants/login';
import { TOAST } from '../../components/Toast/constants';

export const login = ({ login, password }) => {
  return dispatch => {
    return Api.post('/login', { login, password })
      .then(({ data }) => {
        localStorage.setItem('token', data.token);
        localStorage.setItem('user', JSON.stringify(data.user));

        TOAST.sucesso('Login realizado com sucesso!');
        return dispatch({
          type: LOGIN_SUCCESSFUL,
          user: data.user,
          token: data.token,
        });
      })
      .catch(error => {
        TOAST.erro('Login falhou! Revise os dados inseridos e tente novamente!');
        return dispatch({ type: LOGIN_UNSUCCESSFUL });
      });
  };
};

export const logout = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('user');
  return dispatch => {
    dispatch({
      type: LOGOUT,
    });
  };
};
