import { TOAST } from '../../components/Toast/constants';
import {
  GET_ALLOCATIONS_FAILURE,
  GET_ALLOCATIONS_SUCCESS,
  SAVE_ALLOCATIONS_FAILURE,
} from '../../constants/allocations';
import { Api } from '../api/index';
import history from '../../history';

export const getAllocationsByUser = userId => {
  return dispatch => {
    return Api.get(`/user-allocation/${userId}`)
      .then(({ data }) => {
        return dispatch({
          type: GET_ALLOCATIONS_SUCCESS,
          allocations: data,
        });
      })
      .catch(error => {
        TOAST.erro('Erro ao buscar alocações!');
        return dispatch({ type: GET_ALLOCATIONS_FAILURE });
      });
  };
};

export const saveUserAllocation = allocation => {
  return dispatch => {
    return Api.post(`/user-allocation/`, allocation)
      .then(() => {
        TOAST.sucesso('Alocação salva com sucesso!');
        history.push('/');
      })
      .catch(error => {
        TOAST.erro('Erro ao salvar alocação!');
        return dispatch({ type: SAVE_ALLOCATIONS_FAILURE });
      });
  };
};
