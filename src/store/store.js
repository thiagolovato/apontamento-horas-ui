import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
import rootReducer from '../modules/rootReducer';
import { persistReducer } from 'redux-persist';
import localForage from 'localforage';

export const history = createBrowserHistory();

const initialState = {};
const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

let reduxPersistConfig = {
  key: 'token',
  storage: localForage,
  whitelist: ['token'],
};

let reducer = persistReducer(reduxPersistConfig, rootReducer);

const store = createStore(reducer, initialState, composedEnhancers);

export default store;
