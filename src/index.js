import { Container } from '@material-ui/core';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './store/store';
import { Index } from './AppContainer';

function component() {
  let element = document.createElement('div');
  element.id = 'root';
  return element;
}

document.body.appendChild(component());

render(
  <Container maxWidth="lg">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <Provider {...{ store }}>
      <div>
        <Index />
      </div>
    </Provider>
  </Container>,
  document.getElementById('root')
);
