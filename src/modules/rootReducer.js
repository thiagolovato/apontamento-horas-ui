import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import login from '../reducers/login';
import allocations from '../reducers/allocations';
import projects from '../reducers/projects';

const rootReducer = combineReducers({
  routing: routerReducer,
  login,
  allocations,
  projects,
});

export default rootReducer;
