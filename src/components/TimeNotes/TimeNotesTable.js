import { Paper, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import React from 'react';

export const TimeNotesTable = ({ allocations = [] }) => {
  return (
    <Paper>
      <Table size="medium">
        <TableHead>
          <TableRow>
            <TableCell align="left">Projeto</TableCell>
            <TableCell align="right">Data de alocação</TableCell>
            <TableCell align="right">Horas alocadas</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {allocations.map(allocation => (
            <TableRow key={`allocation.id-${allocation.id}`}>
              <TableCell component="th" scope="row">
                {allocation.Project.name}
              </TableCell>
              <TableCell align="right">
                {new Date(allocation.allocationDate).toLocaleDateString('pt-br')}
              </TableCell>
              <TableCell align="right">{allocation.allocationTime}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};
