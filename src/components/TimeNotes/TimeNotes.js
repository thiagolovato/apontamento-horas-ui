import { Grid, IconButton, Typography } from '@material-ui/core';
import { Autorenew } from '@material-ui/icons';
import uniq from 'lodash/uniq';
import moment from 'moment';
import React from 'react';
import { connect } from 'react-redux';
import { getAllocationsByUser } from '../../actions/allocations/index';
import { TimeNotesTable } from './TimeNotesTable';

class TimeNotes extends React.Component {
  componentWillMount() {
    this.loadAllocations();
  }

  loadAllocations() {
    const { getAllocationsByUser, user } = this.props;
    user && getAllocationsByUser(user.id);
  }

  _renderRefreshButton = () => {
    return (
      <IconButton
        color="primary"
        style={{ marginLeft: 'auto' }}
        onClick={() => this.loadAllocations()}
      >
        <Autorenew />
      </IconButton>
    );
  };

  _renderTotalHoursByProject = allocations => {
    const ids = uniq(allocations.map(allocation => allocation.projectId));
    return (
      ids.length > 0 &&
      ids.map(id => {
        const filteredList = allocations.filter(allocation => allocation.projectId === id);
        const [first] = filteredList;
        const sum = filteredList.reduce(
          (acc, allocation) => acc.add(moment.duration(allocation.allocationTime)),
          moment.duration()
        );
        return (
          <Typography variant="subtitle1" key={`first.Project.name-${first.Project.name}`}>
            Horas alocadas no {first.Project.name}:{' '}
            {[Math.floor(sum.asHours()), sum.minutes()].join(':')}
          </Typography>
        );
      })
    );
  };

  _renderAllocationCards = allocations => {
    return (
      <>
        <Grid item xs={12} style={{ display: 'flex' }}>
          <Typography variant="h4">Lista de lançamentos</Typography>
          {this._renderRefreshButton()}
        </Grid>
        <Grid item xs={12}>
          <TimeNotesTable {...{ allocations }} />
        </Grid>
        <Grid item xs={12}>
          {this._renderTotalHoursByProject(allocations)}
        </Grid>
      </>
    );
  };

  render() {
    const { allocations } = this.props;
    const noAllocationsFound = allocations.length === 0;
    return (
      <Grid container spacing={4} style={{ marginBottom: 20 }}>
        {(noAllocationsFound && (
          <>
            <Typography variant="h2" align="center">
              Nenhum registro de alocação encontrado
            </Typography>
            {this._renderRefreshButton()}
          </>
        )) ||
          this._renderAllocationCards(allocations)}
      </Grid>
    );
  }
}

function mapState(state) {
  const {
    allocations: { allocations = [] },
    login: { user },
  } = state || {};
  return { allocations, user };
}

const actionCreators = {
  getAllocationsByUser,
};

const connectedTimeNotes = connect(
  mapState,
  actionCreators
)(TimeNotes);
export { connectedTimeNotes as TimeNotes };
