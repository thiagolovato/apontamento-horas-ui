import { Fab, Grid } from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../actions/login/index';
import { TimeNotes } from './TimeNotes/TimeNotes';
import history from '../history';

class Home extends React.Component {
  render() {
    const { isAdmin } = this.props;
    return (
      <>
        <TimeNotes />
        <Grid item xs={12} style={{ display: 'flex' }}>
          <Fab
            disabled={isAdmin}
            color="primary"
            aria-label="Add"
            style={{ marginLeft: 'auto' }}
            onClick={() => history.push('/allocation')}
          >
            <AddIcon />
          </Fab>
        </Grid>
      </>
    );
  }
}

function mapState(state) {
  const { loggedIn, token, user = {} } = state.login || {};
  return { loggedIn, token, isAdmin: user.isAdmin };
}

const actionCreators = {
  logout,
};

const connectedHome = connect(
  mapState,
  actionCreators
)(Home);
export { connectedHome as Home };
