import { Button, ButtonGroup, Grid, Paper, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';
import { connect } from 'react-redux';
import { saveUserAllocation } from '../../actions/allocations/index';
import { getUserProjects } from '../../actions/projects/index';
import history from '../../history';
import { AllocationDateTimePick } from './AllocationDateTimePick';
import { ProjectSelect } from './ProjectSelect';

class AllocationForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      allocationDate: undefined,
      allocationHour: '01/01/1900 00:00',
      selectedProject: undefined,
    };
  }

  componentWillMount() {
    const { getUserProjects, user } = this.props;
    user.isAdmin && history.push('/');
    getUserProjects(user.id);
  }

  setKey = key => date => {
    this.setState({ [key]: date });
  };

  setProject = value => {
    this.setState({ selectedProject: value });
  };

  saveAllocation = () => {
    const { saveUserAllocation, user } = this.props;
    const { allocationDate = moment().format(), allocationHour, selectedProject } = this.state;
    const allocationHourMoment = moment(allocationHour).format('HH:mm');
    const allocation = {
      allocationDate,
      allocationTime: allocationHourMoment,
      projectId: selectedProject,
      userId: user.id,
    };
    allocationDate && allocationHour && selectedProject && saveUserAllocation(allocation);
  };

  render() {
    const { projects = [] } = this.props;
    const { allocationDate, allocationHour } = this.state;
    return (
      <Paper elevation={4} style={{ padding: 30 }}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant="h4" align="center">
              Registro de alocação
            </Typography>
          </Grid>
          <ProjectSelect
            {...{
              onChange: this.setProject,
              projects,
              selectedProject: this.state.selectedProject,
            }}
          />
          <AllocationDateTimePick {...{ setKey: this.setKey, allocationDate, allocationHour }} />
          <Grid item xs={12}>
            <ButtonGroup fullWidth variant="contained" color="primary">
              <Button onClick={() => this.saveAllocation()}>Salvar</Button>
            </ButtonGroup>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

function mapState(state) {
  const {
    login: { user },
    projects: { projects = [] },
  } = state || {};
  return { user, projects };
}

const actionCreators = {
  getUserProjects,
  saveUserAllocation,
};

const connectedAllocationForm = connect(
  mapState,
  actionCreators
)(AllocationForm);
export { connectedAllocationForm as AllocationForm };
