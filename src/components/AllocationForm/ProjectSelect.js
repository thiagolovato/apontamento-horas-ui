import { FormControl, FormHelperText, InputLabel, MenuItem, Select, Grid } from '@material-ui/core';
import React from 'react';

export const ProjectSelect = ({ projects, onChange, selectedProject }) => {
  return (
    <Grid item xs={12}>
      <FormControl required fullWidth>
        <InputLabel htmlFor="project-required">Projeto</InputLabel>
        <Select
          fullWidth
          value={selectedProject}
          onChange={e => onChange(e.target.value)}
          name="project"
          disabled={projects.length === 0}
          inputProps={{
            id: 'project-required',
          }}
        >
          {projects.map(project => {
            return (
              <MenuItem value={project.projectId} key={`project${project.projectId}`}>
                {project.Project.name}
              </MenuItem>
            );
          })}
        </Select>
        <FormHelperText>Obrigatório</FormHelperText>
      </FormControl>
    </Grid>
  );
};
