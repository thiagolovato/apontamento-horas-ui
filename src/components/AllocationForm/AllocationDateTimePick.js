import MomentUtils from '@date-io/moment';
import { Grid } from '@material-ui/core';
import {
  KeyboardDatePicker,
  KeyboardTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import React from 'react';

export const AllocationDateTimePick = ({ allocationDate, allocationHour, setKey }) => {
  return (
    <>
      <Grid item xs={12} sm={6} md={6}>
        <MuiPickersUtilsProvider utils={MomentUtils} locale={'pt-BR'}>
          <KeyboardDatePicker
            autoOk
            variant="inline"
            inputVariant="outlined"
            label="Data da alocação"
            format="DD/MM/YYYY"
            value={allocationDate}
            InputAdornmentProps={{ position: 'start' }}
            onChange={setKey('allocationDate')}
            invalidDateMessage={'Data inválida'}
          />
        </MuiPickersUtilsProvider>
      </Grid>
      <Grid item xs={12} sm={6} md={6}>
        <MuiPickersUtilsProvider utils={MomentUtils} locale={'pt-BR'}>
          <KeyboardTimePicker
            clearable
            ampm={false}
            variant="inline"
            format="HH:mm"
            label="Horas trabalhadas"
            value={allocationHour}
            onChange={setKey('allocationHour')}
            invalidDateMessage={'Data inválida'}
          />
        </MuiPickersUtilsProvider>
      </Grid>
    </>
  );
};
