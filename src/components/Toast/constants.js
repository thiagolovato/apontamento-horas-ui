import { toast } from 'react-toastify';

export const TOAST_CONFIG = {
  position: toast.POSITION.BOTTOM_RIGHT,
};

export const MENSAGENS_PADRAO = {
  erro: 'Erro desconhecido.',
  sucesso: 'Operação realizada com sucesso.',
};

export const TOAST = {
  erro: (message = MENSAGENS_PADRAO.erro) => {
    toast.error(message, {
      ...TOAST_CONFIG,
      className: 'toast toast--error',
      autoClose: 7000,
    });
  },
  sucesso: (message = MENSAGENS_PADRAO.sucesso) => {
    toast.success(message, {
      ...TOAST_CONFIG,
      className: 'toast toast--success',
      autoClose: 5000,
    });
  },
};
