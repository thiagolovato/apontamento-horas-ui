import { Button, Fade, Paper, Popper, Tab, Tabs } from '@material-ui/core';
import { ExitToApp, History } from '@material-ui/icons';
import React, { useState } from 'react';
import history from '../../history';

export const BottomTabs = ({ logout }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [value, setValue] = useState(0);

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popper' : undefined;

  const handleClick = event => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const handleChangeTab = (event, newVal) => {
    setValue(newVal);
  };

  return (
    <Tabs
      {...{ value }}
      onChange={handleChangeTab}
      indicatorColor="primary"
      textColor="primary"
      centered
      style={{ marginTop: 10 }}
    >
      <Tab label="Visualizar lançamentos" icon={<History />} onClick={() => history.push('/')} />
      <Tab label="Logout" icon={<ExitToApp />} onClick={handleClick} onBlur={handleClick} />
      <Popper
        {...{
          open,
          id,
          anchorEl,
        }}
        transition
      >
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
            <Paper style={{ padding: 30 }}>
              <Button onClick={logout} variant="contained" color="secondary">
                Clique aqui para efetuar o logout.
              </Button>
            </Paper>
          </Fade>
        )}
      </Popper>
    </Tabs>
  );
};
