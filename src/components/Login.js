import { Button, ButtonGroup, Grid, Paper, TextField, Typography } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import history from '../history';
import { login } from '../actions/login/index';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: undefined,
      password: undefined,
      submitted: false,
    };
  }

  componentWillMount() {
    const { loggedIn, token } = this.props;
    if (loggedIn && token) history.push('/');
  }

  setKey = (key, value) => {
    this.setState({ [key]: value });
  };

  componentWillUpdate(newProp, prop) {
    if (newProp.loggedIn && newProp.token && this.state.submitted) {
      history.push('/');
    }
  }

  sendCredentials = async () => {
    const { email, password } = this.state;
    const { login } = this.props;
    if (email && password) {
      this.setState({ submitted: true });
      login({ login: email, password });
    }
  };

  render() {
    return (
      <Paper style={{ padding: 30 }} elevation={2}>
        <Typography variant="h4" gutterBottom align="center">
          Bem-vindo(a) ao sistema de alocação de horas
        </Typography>
        <Grid item xs={12}>
          <TextField
            label="Email"
            style={{ margin: 8 }}
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={e => this.setKey('email', e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            label="Password"
            type="password"
            style={{ margin: 8 }}
            placeholder="Password"
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={e => this.setKey('password', e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <ButtonGroup fullWidth variant="contained" color="primary">
            <Button onClick={this.sendCredentials}>Login</Button>
          </ButtonGroup>
        </Grid>
      </Paper>
    );
  }
}

function mapState(state) {
  const { loggedIn, token } = state.login || {};
  return { loggedIn, token };
}

const actionCreators = {
  login,
};

const connectedLogin = connect(
  mapState,
  actionCreators
)(Login);
export { connectedLogin as Login };
