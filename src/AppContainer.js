import React from 'react';
import { connect } from 'react-redux';
import { Route, Router, Switch } from 'react-router-dom';
import history from './history';
import { BottomTabs } from './components/BottomTabs/Tabs';
import { Home } from './components/Home';
import { Login } from './components/Login';
import { PrivateRoute } from './components/PrivateRoute';
import { ToastComponent } from './components/Toast/Toast';
import { logout } from './actions/login/index';
import { Paper } from '@material-ui/core';
import { AllocationForm } from './components/AllocationForm/AllocationForm';

class Index extends React.Component {
  logout = () => {
    const { logout } = this.props;
    logout();
    history.push('/login');
  };

  render() {
    const { loggedIn, token } = this.props;
    return (
      <main>
        <Paper style={{ padding: 30 }} elevation={2}>
          <Router {...{ history }}>
            <Switch>
              <Route path="/login" exact component={Login} key={'Login-route'} />
              <PrivateRoute path="/" exact component={Home} key={'Home-route'} />
              <PrivateRoute
                path="/allocation"
                exact
                component={AllocationForm}
                key={'AllocationForm-route'}
              />
            </Switch>
            {loggedIn && token && <BottomTabs logout={this.logout} />}
          </Router>
          <ToastComponent />
        </Paper>
      </main>
    );
  }
}

const mapState = state => {
  const { loggedIn, token } = state.login || {};
  return { loggedIn, token };
};

const actionCreators = {
  logout,
};

const connectedIndex = connect(
  mapState,
  actionCreators
)(Index);
export { connectedIndex as Index };
