
const error = (state = [], {type, error}) => {
  switch (type) {
  case 'API_ERROR':
    return Object.assign({}, state, {
      error
    });
  default: 
    return state;
  }
  
};

export default error;