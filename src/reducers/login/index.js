import { USER_LOGIN, LOGIN_SUCCESSFUL, LOGIN_UNSUCCESSFUL, LOGOUT } from '../../constants/login';

let token = localStorage.getItem('token');
let user = JSON.parse(localStorage.getItem('user'));
const initialState = token ? { loggedIn: true, token, user } : {};

const login = (state = initialState, { type, user, token }) => {
  switch (type) {
    case USER_LOGIN:
      return Object.assign({}, state, {
        user,
      });
    case LOGIN_SUCCESSFUL:
      return Object.assign({}, state, {
        loggedIn: true,
        token,
        user,
      });
    case LOGIN_UNSUCCESSFUL:
      return Object.assign({}, state, {
        loggedIn: false,
        token: undefined,
      });
    case LOGOUT:
      return Object.assign({}, state, {
        loggedIn: false,
        token: undefined,
      });
    default:
      return state;
  }
};

export default login;
