import { GET_ALLOCATIONS_SUCCESS, SAVE_ALLOCATIONS_SUCCESS } from '../../constants/allocations';

const allocations = (state = [], { type, allocations }) => {
  switch (type) {
    case GET_ALLOCATIONS_SUCCESS:
      return Object.assign({}, state, {
        allocations,
      });
    case SAVE_ALLOCATIONS_SUCCESS:
      state.allocations.concat(allocations);
      return Object.assign({}, state, {
        allocations,
      });
    default:
      return state;
  }
};

export default allocations;
