import { GET_PROJECTS_SUCCESS } from '../../constants/projects';

const projects = (state = [], { type, projects }) => {
  switch (type) {
    case GET_PROJECTS_SUCCESS:
      return Object.assign({}, state, {
        projects,
      });
    default:
      return state;
  }
};

export default projects;
